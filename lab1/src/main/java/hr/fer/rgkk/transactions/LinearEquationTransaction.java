package hr.fer.rgkk.transactions;

import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.script.Script;
import org.bitcoinj.script.ScriptBuilder;

import static org.bitcoinj.script.ScriptOpCodes.*;

/**
 * You must implement locking and unlocking script such that transaction output is locked by 2 integers x and y
 * such that they are solution to the equation system:
 * <pre>
 *     x + y = first four digits of your student id
 *     abs(x-y) = last four digits of your student id
 * </pre>
 * If needed change last digit of your student id such that x and y have same parity. This is needed so that equation
 * system has integer solutions.
 */
public class LinearEquationTransaction extends ScriptTransaction {

    private final int jmbag_start;
    private final int jmbag_end;
    private final int x;
    private final int y;

    public LinearEquationTransaction(WalletKit walletKit, NetworkParameters parameters) {
        super(walletKit, parameters);

        //  0036497867
        // -         1
        // -----------
        //  0036497866
        jmbag_start = 36;
        jmbag_end = 7866;

        //  x + y  =   36
        // |x - y| = 7866
        // --------------
        // (x, y) = (3951, -3915) or (-3915, 3951)
        // Let (x, y) be (3951, -3915)
        x = 3951;
        y = -3915;
    }

    @Override
    public Script createLockingScript() {
        return new ScriptBuilder()
                // Verify first equation
                .op(OP_2DUP)
                .op(OP_ADD)
                .number(jmbag_start)
                .op(OP_EQUALVERIFY)
                // Verify second equation
                .op(OP_SUB)
                .op(OP_ABS)
                .number(jmbag_end)
                .op(OP_EQUAL)
                // "OP_2DUP OP_ADD <jmbag_start> OP_EQUALVERIFY OP_SUB OP_ABS
                // <jmbag_end> OP_EQUAL"
                .build();
    }

    @Override
    public Script createUnlockingScript(Transaction unsignedScript) {
        return new ScriptBuilder()
                .number(x)
                .number(y)
                // "<x> <y>"
                .build();
    }
}
