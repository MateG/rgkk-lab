package hr.fer.rgkk.transactions;

import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.core.Utils;
import org.bitcoinj.crypto.TransactionSignature;
import org.bitcoinj.script.Script;
import org.bitcoinj.script.ScriptBuilder;

import java.security.SecureRandom;

import static org.bitcoinj.script.ScriptOpCodes.*;

/**
 * You must implement Odds and Evens game between two players such that only
 * the winner can unlock the coins.
 */
public class OddsAndEvens extends ScriptTransaction {

    // Alice's private key
    private final ECKey aliceKey;
    // Alice's nonce
    private final byte[] aliceNonce;
    // Bob's private key
    private final ECKey bobKey;
    // Bob's nonce
    private final byte[] bobNonce;

    // Key used in unlocking script to select winning player.
    // Winner is preselected here so that we can run tests.
    private final ECKey winningPlayerKey;

    private OddsAndEvens(
            WalletKit walletKit, NetworkParameters parameters,
            ECKey aliceKey, byte[] aliceNonce,
            ECKey bobKey, byte[] bobNonce,
            ECKey winningPlayerKey
    ) {
        super(walletKit, parameters);
        this.aliceKey = aliceKey;
        this.aliceNonce = aliceNonce;
        this.bobKey = bobKey;
        this.bobNonce = bobNonce;
        this.winningPlayerKey = winningPlayerKey;
    }

    @Override
    public Script createLockingScript() {
        return new ScriptBuilder()
                // Verify that Alice's nonce corresponds to Alice's commitment
                .op(OP_DUP)
                .op(OP_HASH160)
                .data(Utils.sha256hash160(aliceNonce))
                .op(OP_EQUALVERIFY)
                // Verify that Bob's nonce corresponds to Bob's commitment
                .op(OP_OVER)
                .op(OP_HASH160)
                .data(Utils.sha256hash160(bobNonce))
                .op(OP_EQUALVERIFY)
                // Calculate Alice's N [0-1]
                .op(OP_SIZE)
                .op(OP_NIP)
                .op(OP_16)
                .op(OP_SUB)
                // Calculate Bob's N [0-1]
                .op(OP_SWAP)
                .op(OP_SIZE)
                .op(OP_NIP)
                .op(OP_16)
                .op(OP_SUB)
                // If N's are equal, then 1 (even wins), else 0 (odd wins)
                .op(OP_EQUAL)
                .data(aliceKey.getPubKey())
                .op(OP_SWAP)
                .data(bobKey.getPubKey())
                .op(OP_SWAP)
                // Move the winner's public key to the top of stack
                .op(OP_ROLL)
                // Remove the loser's public key
                .op(OP_NIP)
                // Check if signature verifies with winner's public key
                .op(OP_CHECKSIG)
                // "OP_DUP OP_HASH160 <RA_expected> OP_EQUALVERIFY OP_OVER
                // OP_HASH160 <RB_expected> OP_EQUALVERIFY OP_SIZE OP_NIP OP_16
                // OP_SUB OP_SWAP OP_SIZE OP_NIP OP_16 OP_SUB OP_EQUAL
                // <alicePubKey> OP_SWAP <bobPubKey> OP_SWAP OP_ROLL OP_NIP
                // OP_CHECKSIG"
                .build();
    }

    @Override
    public Script createUnlockingScript(Transaction unsignedTransaction) {
        TransactionSignature signature = sign(unsignedTransaction, winningPlayerKey);
        return new ScriptBuilder()
                .data(signature.encodeToBitcoin())
                .data(bobNonce)   // Odds player
                .data(aliceNonce) // Evens player
                // "<sig> <RB> <RA>"
                .build();
    }

    public static OddsAndEvens of(
            WalletKit walletKit, NetworkParameters parameters,
            OddsEvenChoice aliceChoice, OddsEvenChoice bobChoice,
            WinningPlayer winningPlayer
    ) {
        byte[] aliceNonce = randomBytes(16 + aliceChoice.value);
        byte[] bobNonce = randomBytes(16 + bobChoice.value);

        ECKey aliceKey = randKey();
        ECKey bobKey = randKey();

        // Alice is EVEN, bob is ODD
        ECKey winningPlayerKey = WinningPlayer.EVEN == winningPlayer ? aliceKey : bobKey;

        return new OddsAndEvens(
                walletKit, parameters,
                aliceKey, aliceNonce,
                bobKey, bobNonce,
                winningPlayerKey
        );
    }

    private static byte[] randomBytes(int length) {
        SecureRandom random = new SecureRandom();
        byte[] bytes = new byte[length];
        random.nextBytes(bytes);
        return bytes;
    }

    public enum WinningPlayer {
        ODD, EVEN
    }

    public enum OddsEvenChoice {

        ZERO(0),
        ONE(1);

        public final int value;

        OddsEvenChoice(int value) {
            this.value = value;
        }
    }
}

