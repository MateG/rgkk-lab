package hr.fer.rgkk.transactions;

import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.script.Script;
import org.bitcoinj.script.ScriptBuilder;

import static org.bitcoinj.script.ScriptOpCodes.*;

/**
 * You must implement standard Pay-2-Public-Key-Hash transaction type.
 */
public class PayToPubKeyHash extends ScriptTransaction {

    private final ECKey key = randKey();

    public PayToPubKeyHash(WalletKit walletKit, NetworkParameters parameters) {
        super(walletKit, parameters);
    }

    @Override
    public Script createLockingScript() {
        return new ScriptBuilder()
                // Verify that the hash of public key matches
                .op(OP_DUP)
                .op(OP_HASH160)
                .data(key.getPubKeyHash())
                .op(OP_EQUALVERIFY)
                // Check if signature verifies with public key
                .op(OP_CHECKSIG)
                // "OP_DUP OP_HASH160 <pubKeyHash> OP_EQUALVERIFY OP_CHECKSIG"
                .build();
    }

    @Override
    public Script createUnlockingScript(Transaction unsignedTransaction) {
        return new ScriptBuilder()
                .data(sign(unsignedTransaction, key).encodeToBitcoin())
                .data(key.getPubKey())
                // "<sig> <pubKey>"
                .build();
    }
}
