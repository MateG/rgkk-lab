pragma solidity ^0.4.24;

import "./Auction.sol";

contract EnglishAuction is Auction {

    uint internal highestBid;
    uint internal initialPrice;
    uint internal biddingPeriod;
    uint internal lastBidTimestamp;
    uint internal minimumPriceIncrement;

//    address internal highestBidder;

    constructor(
        address _sellerAddress,
        address _judgeAddress,
        Timer _timer,
        uint _initialPrice,
        uint _biddingPeriod,
        uint _minimumPriceIncrement
    ) public Auction(_sellerAddress, _judgeAddress, _timer) {
        initialPrice = _initialPrice;
        biddingPeriod = _biddingPeriod;
        minimumPriceIncrement = _minimumPriceIncrement;
        lastBidTimestamp = time();
    }

    function bid() public payable {
        require(outcome == Outcome.NOT_FINISHED, "Auction finished");

        uint256 currentTime = time();
        require(currentTime < lastBidTimestamp + biddingPeriod, "Bidding period expired");

        bool firstBid = highestBidderAddress == address(0);
        uint minimumBid = firstBid ? initialPrice : highestBid + minimumPriceIncrement;
        require(msg.value >= minimumBid, "Bid too low");

        if (!firstBid) {
            highestBidderAddress.transfer(highestBid);
        }

        highestBid = msg.value;
        highestBidderAddress = msg.sender;
        lastBidTimestamp = currentTime;
    }

    function getHighestBidder() public returns (address) {
        return time() >= lastBidTimestamp + biddingPeriod ? highestBidderAddress : address(0);
    }
}
